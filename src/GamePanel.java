import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Random;

public class GamePanel extends JPanel implements ActionListener, KeyListener {

    private boolean left = false;
    private boolean up = false;
    private boolean right= true;
    private boolean down = false;
    private boolean gameOver = false;


    static final int PANEL_WIDTH = 550;
    static final int PANEL_HEIGHT = 850;

    private int[] snakexLength = new int[750];
    private int[] snakeyLength = new int[750];
    private int lengthOfSnake = 3;
    int appleX,appleY,score;

    private int moves = 0;
    private final int UNIT_SIZE = 25;
    private Timer timer;
    private int delay = 75;
    Random random = new Random();

    private ImageIcon snaketitle = new ImageIcon(getClass().getResource("assets/snaketitle.jpg"));
    private ImageIcon apple = new ImageIcon(getClass().getResource("assets/enemy.png"));
    private ImageIcon leftmouth = new ImageIcon(getClass().getResource("assets/leftmouth.png"));
    private ImageIcon rightmouth = new ImageIcon(getClass().getResource("assets/rightmouth.png"));
    private ImageIcon snakeimage = new ImageIcon(getClass().getResource("assets/snakeimage.png"));
    private ImageIcon upmouth = new ImageIcon(getClass().getResource("assets/upmouth.png"));
    private ImageIcon downmouth = new ImageIcon(getClass().getResource("assets/downmouth.png"));


    GamePanel(){
        setBackground(Color.darkGray);
        addKeyListener(this);
        setFocusable(true);
        setFocusTraversalKeysEnabled(true);
        timer = new Timer(delay,this);
        timer.start();

        newApple();
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        // Grid game
       /* for (int i = 0; i < PANEL_HEIGHT/UNIT_SIZE; i++) {
            g.setColor(Color.black);
            g.drawLine(25+(i * UNIT_SIZE), 75, 25+(i * UNIT_SIZE), 75+PANEL_WIDTH);
        }
        for (int j = 0; j < PANEL_WIDTH/UNIT_SIZE; j++) {
            g.setColor(Color.black);
            g.drawLine(25, 75+(j * UNIT_SIZE), 25+PANEL_HEIGHT,75+(j * UNIT_SIZE));
        }*/

        // Top rectangle
        g.setColor(Color.white);
        g.drawRect(24,10,851,55);

        snaketitle.paintIcon(this,g,25,11);
        // Core rectangle
        g.setColor(Color.white);
        g.drawRect(24,74,PANEL_HEIGHT+1,PANEL_WIDTH+1);
        g.setColor(Color.black);
        g.fillRect(25,75,PANEL_HEIGHT,PANEL_WIDTH);
        // Game stat
        g.setFont(new Font("Arial",Font.BOLD,12));
        g.drawString("Score= "+score,750,30);

        g.setFont(new Font("Arial",Font.BOLD,12));
        g.drawString("Length= "+lengthOfSnake,750,50);

        //  Initialize apple
        apple.paintIcon(this,g,appleX,appleY);

        if (moves==0){
            // Initialize snake position
            snakexLength[0]=100;
            snakexLength[1]=75;
            snakexLength[2]=50;

            snakeyLength[0]=100;
            snakeyLength[1]=100;
            snakeyLength[2]=100;
            moves++;
        }

        // Head's snake position
        if (left){
            leftmouth.paintIcon(this,g,snakexLength[0],snakeyLength[0]);
        }
        if (right){
            rightmouth.paintIcon(this,g,snakexLength[0],snakeyLength[0]);
        }
        if (up){
            upmouth.paintIcon(this,g,snakexLength[0],snakeyLength[0]);
        }
        if (down){
            downmouth.paintIcon(this,g,snakexLength[0],snakeyLength[0]);
        }
        for (int i = 1; i < lengthOfSnake; i++) {
            snakeimage.paintIcon(this,g,snakexLength[i],snakeyLength[i]);
        }
        if (gameOver){
            g.setColor(Color.white);
            g.setFont(new Font("Arial",Font.BOLD,50));
            g.drawString("Game Over",300,300);

            g.setFont(new Font("Arial",Font.PLAIN,20));
            g.drawString("Press \"Space\" to restart",320,350);
        }
        g.dispose();
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        for (int i = lengthOfSnake-1; i > 0; i--) {
            snakexLength[i] = snakexLength[i-1];
            snakeyLength[i] = snakeyLength[i-1];
        }
        if (right){
            snakexLength[0] = snakexLength[0]+25;
        }
        if (left){
            snakexLength[0] = snakexLength[0]-25;
        }
        if (up){
            snakeyLength[0] = snakeyLength[0]-25;
        }
        if (down){
            snakeyLength[0] = snakeyLength[0]+25;
        }

        // Border limits
        if (snakexLength[0] > PANEL_HEIGHT) {
            snakexLength[0] = 25;
        }
        if (snakeyLength[0] >= PANEL_WIDTH+75){
            snakeyLength[0] = 75;
        }
        if (snakeyLength[0] < 75){
            snakeyLength[0] = PANEL_WIDTH+50;
        }
        if (snakexLength[0] < 25) {
            snakexLength[0] = PANEL_HEIGHT;
        }
        checkApple();
        checkSnake();
        repaint();
    }
    public void newApple(){
        appleX  = 25 + random.nextInt(PANEL_HEIGHT/UNIT_SIZE)*UNIT_SIZE;
        appleY  = 75 + random.nextInt(PANEL_WIDTH/UNIT_SIZE)*UNIT_SIZE;
    }

    public void checkApple(){
        if (snakexLength[0] == appleX && snakeyLength[0] == appleY) {
            newApple();
            lengthOfSnake++;
            score++;
        }
    }

    public void checkSnake(){
        for (int i = lengthOfSnake; i > 0; i--) {
            if (snakexLength[0] == snakexLength[i] && snakeyLength[0] == snakeyLength[i]) {
                timer.stop();
                gameOver = true;
            }
        }
    }

    public void restart(){
        lengthOfSnake=3;
        gameOver=false;
        score=0;
        moves=0;
        up=false;
        down=false;
        right=true;
        left=false;
        timer.start();
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }
    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()){
                case KeyEvent.VK_LEFT:
                    if (!right){
                        up=false;
                        down=false;
                        right=false;
                        left=true;
                        moves++;}
                    break;
                case KeyEvent.VK_RIGHT:
                    if (!left){
                        up=false;
                        down=false;
                        right=true;
                        left=false;
                        moves++;}
                    break;
                case KeyEvent.VK_UP:
                    if(!down){
                        up=true;
                        down=false;
                        right=false;
                        left=false;
                        moves++;}
                    break;
                case KeyEvent.VK_DOWN:
                    if(!up){
                        down=true;
                        up=false;
                        right=false;
                        left=false;
                        moves++;}
                    break;
            case KeyEvent.VK_SPACE:
                restart();
                break;
            }
        }

    @Override
    public void keyReleased(KeyEvent e) {

    }

}
